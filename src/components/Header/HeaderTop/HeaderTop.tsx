import { Container } from "@material-ui/core";
import React from "react";
import styled from "styled-components";
import HeaderLogo from "./HeaderLogo";
import HeaderTopRight from "./HeaderTopRight";

const HeaderTopBox = styled("div")({
  paddingTop: "26px",
  paddingBottom: "13px",
  backgroundColor: "#F3F4F5",
  position: "relative",
  "&:before": {
    content: "fdgdfgdfg",
    backgroundImage: "url(../images/header-bg-1.png)",
    left: 0,
    backgroundPosition: "top left",
    width: "293px",
  },
  ".container": {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
});

const HeaderTop = () => {
  return (
    <HeaderTopBox>
      <Container>
        <HeaderLogo />
        <HeaderTopRight />
      </Container>
    </HeaderTopBox>
  );
};

export default HeaderTop;
