import Image from "next/image";
import Link from "next/link";
import React from "react";

const HeaderLogo = () => {

  return (
    <Link href="/">
      <a>
        <Image src="/logo.png" width={330} height={66} alt="Logo" />
      </a>
    </Link>
  );
};

export default HeaderLogo;
