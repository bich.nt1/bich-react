import React from "react";
import styled from "styled-components";
import HeaderMenu from "./HeaderMenu/HeaderMenu";
import HeaderTop from "./HeaderTop/HeaderTop";

const HeaderBox = styled("div")({});

const HeaderContainer = () => {
  return <HeaderBox>
    <HeaderTop />
    <HeaderMenu />
  </HeaderBox>;
};

export default HeaderContainer;
