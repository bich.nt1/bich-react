import { Container } from '@material-ui/core';
import React from 'react';
import styled from 'styled-components';
import HeaderList from './HeaderList';

const HeaderMenuBox = styled("div") ({

})

const HeaderMenu = () => {
    return (
        <HeaderMenuBox>
            <Container>
                <HeaderList />
            </Container>
        </HeaderMenuBox>
    );
}

export default HeaderMenu;