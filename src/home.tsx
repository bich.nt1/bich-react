import React from 'react';
import HeaderContainer from './components/Header/HeaderContainer';

const Home = () => {
    return (
        <>
            <HeaderContainer />
        </>
    );
}

export default Home;